Pour lancer l'infra :

cd infrastructures

cp .env.dist .env

sudo echo $(docker network inspect bridge | grep Gateway | grep -o -E '([0-9]{1,3}\.){3}[0-9]{1,3}') "kontakt.local" >> /etc/hosts

dans etc/hosts mettre 127.0.0.1 pour kontakt.local

docker-compose build

docker-compose up


docker exec -it nomcontainer bash

